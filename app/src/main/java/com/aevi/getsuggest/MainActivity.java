package com.aevi.getsuggest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private final int AEVI_REQUEST_CODE = 11001;
    @BindView(R.id.autocomplete)
    Button suggestButton;
    @BindView(R.id.edit_text)
    EditText editText;
    @BindView(R.id.suggestion)
    TextView suggestionText;
    @BindView(R.id.clearcomplete)
    Button clearSuggestions;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.clearcomplete)
    public void onClearSuggestionClick() {
        suggestionText.setText(getString(R.string.suggestions));

    }

    @OnClick(R.id.autocomplete)
    public void onSuggestClick() {
        if (TextUtils.isEmpty(editText.getText())) {
            Toast.makeText(this, R.string.enter_text, Toast.LENGTH_SHORT).show();
            return;
        }

        Intent suggestIntent = new Intent(Intent.ACTION_SEND);
        suggestIntent.addCategory(Intent.CATEGORY_DEFAULT);
        suggestIntent.putExtra("AEVI_QUERY", editText.getText().toString());
        suggestIntent.setType("text/plain");
        startActivityForResult(suggestIntent, AEVI_REQUEST_CODE);
        //startActivity(Intent.createChooser(suggestIntent, getString(R.string.guide_text)));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case AEVI_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    List<String> resultList = data.getStringArrayListExtra("AEVI_RESULT");
                    if (resultList != null && !resultList.isEmpty()) {
                        for (String item : resultList) {
                            suggestionText.append(item + ",");
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
