# GetSuggest #

GetSuggest is demo app provided to work in conjuntion with WordComplete (available at repo : git@bitbucket.org:AbhiHolkar/wordcomplete.git)
Application provides option to get Suggestion for word match based on data set(word file) available at WordComplete.
To get Suggestion click on "Suggest!" button and to clear suggestion(s) click on "Clear" button

# Prerequiste

GetSuggest application works in conjunction with WordComplete app. Please install this app first.







